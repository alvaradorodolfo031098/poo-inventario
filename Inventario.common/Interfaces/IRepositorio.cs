﻿using Inventario.common.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.common.Interfaces
{
   public interface IRepositorio<T> where T:Base
    {
        bool Create(T Entidad);
        bool Update(string Id, T EntidadModificada);
        bool Delete(string Id);
        List<T> Read { get; }
    }
}
