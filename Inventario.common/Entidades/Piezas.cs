﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.common.Entidades
{
  public  class Piezas:Base
    {
        public string Nombre { get; set; }
      
        public string Categoria { get; set; }
        public string Descripcion { get; set; }
    }
}
