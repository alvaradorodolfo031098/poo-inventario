﻿using Inventario.common.Entidades;
using Inventario.common.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.DAL
{
    public class RepositorioDeEmpleados : IRepositorio<Empleado>
    {
        private string DBName = "Inventario.db";
        private string TableName = "Empleados";

        public List<Empleado> Read
        {
            get
            {
                List<Empleado> datos = new List<Empleado>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Empleado>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(Empleado Entidad)
        {
            Entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Empleado>(TableName);
                    coleccion.Insert(Entidad);
                }
                    
                    return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public bool Delete(string Id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Empleado>(TableName);
                   r= coleccion.Delete(e => e.Id==Id);
                }

                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(string Id, Empleado EntidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Empleado>(TableName);
                    coleccion.Update(EntidadModificada);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
